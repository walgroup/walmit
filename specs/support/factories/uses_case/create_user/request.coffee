Request = require '../../../../../src/uses_case/create_user/request'

params =
  city: 'city'
  month: 9
  year: '1980'
  gender: 'M'
  nick: 'Nick'
  mail: 'test@server.com'
  search:
    city: 'city_search'
    year_from: 1981
    year_to: 1982
    gender: 'M'

factory 'create_user_request', class: Request, ->
  createWith params

  trait 'with_matchers', ->
    set 'matchers', -> ['123456', 'abcde']
