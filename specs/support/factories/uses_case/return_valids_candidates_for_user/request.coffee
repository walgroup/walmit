Request = require '../../../../../src/uses_case/return_valids_candidates_for_user/request'

params =
  city: 'city'
  month: 9
  year: '1980'
  gender: 'M'
  nick: 'Nick'
  mail: 'test@server.com'
  search:
    city: 'city_search'
    year_from: 1981
    year_to: 1982
    gender: 'M'

factory 'return_valids_candidates_for_user_request', class: Request, ->
  createWith params

