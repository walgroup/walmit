Request = require '../../../../../src/uses_case/send_validation_email_user/request'

factory 'send_validation_email_user_request', class: Request, ->
  createWith "123", "walmitcom@gmail.com"
