User = require '../../../../src/entities/user'

params =
  city: 'city'
  month: 9
  year: '1980'
  gender: 'M'
  nick: 'Nick'
  mail: 'test@server.com'
  password: '123'
  tc: true
  search:
    city: 'city_search'
    year_from: 1981
    year_to: 1982
    gender: 'M'

factory 'user', class: User, ->
  createWith params

  trait 'with_matchers', ->
    set 'matchers', -> ['123456', 'abcde']
