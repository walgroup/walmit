charly =
  city: 'Barcelona'
  month: 9
  year: 1981
  gender: 'M'
  nick: 'Charly'
  mail: 'test@server.com'
  password: '123'
  tc: true
  search:
    city: 'Barcelona'
    year_from: 29
    year_to: 35
    gender: 'F'

nay =
  city: 'Barcelona'
  month: 9
  year: 1979
  gender: 'F'
  nick: 'Nayme'
  mail: 'test@server.com'
  password: '123'
  tc: true
  search:
    city: 'Barcelona'
    year_from: 30
    year_to: 36
    gender: 'M'

kathy =
  city: 'Barcelona'
  month: 9
  year: 1991
  gender: 'F'
  nick: 'Kathy'
  mail: 'test@server.com'
  password: '123'
  tc: true
  search:
    city: 'Barcelona'
    year_from: 30
    year_to: 36
    gender: 'M'

sarah =
  city: 'Madrid'
  month: 9
  year: 1979
  gender: 'F'
  nick: 'Sarah'
  mail: 'test@server.com'
  password: '123'
  tc: true
  search:
    city: 'Barcelona'
    year_from: 30
    year_to: 36
    gender: 'M'

module.exports = [charly, nay, kathy, sarah]
