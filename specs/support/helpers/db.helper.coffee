async = require 'async'
mongojs = require 'mongojs'
config = require '../../../config'

##
# Helper para limpiar la base de datos antes de hacer nuevos tests
# @param {Object} db referencia a la base de datos
# @param {Function} cb callback llamado al eliminar la base de datos
##
spectacular.helper 'dropDatabaseHelper', (cb) ->
  mongojs(config.mongo.db).dropDatabase cb

##
# Helper crea y devuelve una nueva base de datos con las collecciones indicadas
# @param {String} db mongodb data base string, referente al nombre de la base de datos
# @param {Array<String>} collections Array que indicar el nombre nombre de las collecciones a crear
# @return {Object} referencia a la conexion de la base de datos
##
spectacular.helper 'createDatabaseHelper', (db = config.mongo.db, collections = ['users']) ->
  mongojs db, collections

##
# Helper para salvar cualquier documento en la colleccion indicada sin ningun tipo de ciudad. Usar únicamente en tests
# @param {Object|Array} docs objecto o array de objetos a guardar en la base de datos
# @param {String} collection colleccion sobre la que se va a guardar el documento.
# @param {Function} cb devuelve tanto el error como el documento resultante. (err, doc)
# @param {String} db mongodb data base string, referente al nombre de la base de datos. Por defecto toma la cadena de coneccion de la configuracion
##
spectacular.helper 'saveDocumentDatabaseHelper', (docs, collection, cb, db = config.mongo.db) ->
  docs = if docs instanceof Array then docs else [docs]
  collection = mongojs(config.mongo.db, [collection])[collection]
  users = []
  save = (doc, cb) ->
    delete doc._id
    collection.save doc, (err, res) ->
      users.push res
      cb err

  async.each docs, save, (err) ->
    cb err, users
