require '../../support/factories/entities/user'
crypto = require 'crypto'
HashMail = require '../../../src/libs/hash'

cipher = crypto.createCipher 'aes256', 'Un mundo feliz'

describe HashMail, ->

  it -> should exist
  itsInstance -> should exist

  describe '::create', ->
    context 'llamado con una entidad User valida', ->
      itsReturn with: ['123', 'test@server.com'], -> should equal (cipher.update("123#test@server.com", 'utf8', 'hex') + cipher.final('hex'))

  describe '::check', ->
    user = null
    before (async) ->
      dropDatabaseHelper ->
        saveDocumentDatabaseHelper {email: 'test@server.com'}, 'users', (err, doc) ->
          user = doc
          async.resolve()

    after (async) ->
      dropDatabaseHelper -> async.resolve()
    context 'llamando con un hash válido', ->
      it 'el callback debe devolver yes', (async) ->
        hash = new HashMail().create( user._id, user.email )
        @subject hash, (ok) ->
          expect(ok).to equal yes
          async.resolve()
    context 'llamado con un hash inválido', ->
      it 'el callback debe devolver no', (async) ->
        @subject '123456789', (ok) ->
          expect(ok).to equal no
          async.resolve()
