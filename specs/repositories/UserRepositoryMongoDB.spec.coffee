require '../support/factories/entities/user'
UserRepository = require '../../src/repositories/user/userRepositoryMongoDB'

describe UserRepository, ->
  before (async) ->
    dropDatabaseHelper -> async.resolve()
    
  it 'the class', -> should exist
  itsInstance -> should exist

  whenPass ->
    describe '::save', ->
      context 'called with a correct User Entity instance', ->
        it 'the error', (async) ->
          @subject create('user'), (err, usrSave) ->
            expect(err).to equal null
            async.resolve()
        it 'the document', (async)->
          @subject create('user'), (err, usrSave) ->
            expect(usrSave.id()).to exist
            async.resolve()
