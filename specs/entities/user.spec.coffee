User = require '../../src/entities/user'

describe User, ->
  withArguments
    city: 'city'
    month: 9
    year: '1980'
    gender: 'M'
    nick: 'Nick'
    mail: 'test@server.com'
    password: '123'
    tc: true
    search:
      city: 'city_search'
      year_from: 1981
      year_to: 1982
      gender: 'M'

  it -> should exist
  itsInstance -> should exist

  describe '::id', ->
    context 'called with 123', ->
      itsReturn with: ['123'], ->
        should equal true
    context 'called with null id', ->
      itsReturn with:[null], ->
        should equal '123'
