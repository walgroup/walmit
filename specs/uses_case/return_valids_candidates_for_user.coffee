require '../support/factories/uses_case/return_valids_candidates_for_user/request'
require '../support/factories/repositories/userRespositoryMongoDB'
users = require '../support/fixtures/users'

async = require 'async'

ReturnsValidsCandidatesForUserUseCase = require '../../src/uses_case/return_valids_candidates_for_user'
ReturnsValidsCandidatesForUserRequest = require '../../src/uses_case/return_valids_candidates_for_user/request'

describe ReturnsValidsCandidatesForUserUseCase, ->
  withArguments create 'userRepositoryMongoDB'

  it -> should exist
  itsInstance -> should exist
  
  whenPass ->
    describe '::execute', ->
      usersCreated = null; req = null; reqId = null
      context 'with a valid request instance', ->
        before (promise) ->
          dropDatabaseHelper ->
            saveDocumentDatabaseHelper users, 'users', (err, docs) ->
              usersCreated = docs
              req = new ReturnsValidsCandidatesForUserRequest usersCreated[0]
              reqId = new ReturnsValidsCandidatesForUserRequest "#{usersCreated[0]._id}", create('userRepositoryMongoDB')
              promise.resolve()

        after (promise) ->
          dropDatabaseHelper -> promise.resolve()

        xit 'the error must be null', (async) ->
          @subject req, (err, res) ->
            expect(err).to equal null
            async.resolve()

        xit 'users matches must be just one, Nayme', (async) ->
          @subject req, (err, res) ->
            expect(res.user.matchers().length).to equal 1
            expect("#{res.user.matchers()[0]}").to equal "#{usersCreated[1]._id}"
            async.resolve()

      context 'with a request with just the id of the user', ->
        before (promise) ->
          dropDatabaseHelper ->
            saveDocumentDatabaseHelper users, 'users', (err, docs) ->
              usersCreated = docs
              req = new ReturnsValidsCandidatesForUserRequest usersCreated[0]
              reqId = new ReturnsValidsCandidatesForUserRequest "#{usersCreated[0]._id}", create('userRepositoryMongoDB')
              promise.resolve()

        after (promise) ->
          dropDatabaseHelper -> promise.resolve()

        it 'users matches must be just one, Nayme', (async) ->
          @subject reqId, (err, res) ->
            expect(res.user.matchers().length).to equal 1
            expect("#{res.user.matchers()[0]}").to equal "#{usersCreated[1]._id}"
            async.resolve()


