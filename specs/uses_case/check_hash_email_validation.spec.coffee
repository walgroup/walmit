HashMail = require '../../src/libs/hash'
require '../support/factories/uses_case/check_hash_email_validation/request'

CheckHashValidationUseCase = require '../../src/uses_case/check_hash_email_validation'
CheckHashValidationRequest = require '../../src/uses_case/check_hash_email_validation/request'

describe CheckHashValidationUseCase, ->
  user = null
  before (async) ->
    dropDatabaseHelper ->
      saveDocumentDatabaseHelper {email: 'test@server.com'}, 'users', (err, doc) ->
        user = doc
        async.resolve()

  after (async) ->
    dropDatabaseHelper -> async.resolve()

  it -> should exist
  itsInstance -> should exist

  describe '::execute', ->
    context 'llamandole con una instancia validad de CheckHashValidationRequest', ->
      it 'debe llamarse al callback con true', (async) ->
        hash = new HashMail().create( user._id, user.email )
        @subject new CheckHashValidationRequest(hash), (ok) ->
          expect(ok).to equal yes
          async.resolve()
