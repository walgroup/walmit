require '../support/factories/uses_case/create_user/request'
require '../support/factories/repositories/userRespositoryMongoDB'

CreateUserUseCase = require '../../src/uses_case/create_user'
CreateUserRequest = require '../../src/uses_case/create_user/request'

describe CreateUserUseCase, ->
  withArguments create 'userRepositoryMongoDB'

  before (async)->
    dropDatabaseHelper -> async.resolve()

  after (async)->
    dropDatabaseHelper -> async.resolve()

  it -> should exist
  itsInstance -> should exist
  
  whenPass ->
    describe '::execute', ->
      context 'called with an instance of CreateUserRequest', ->
        it 'the error', (async) ->
          @subject create('create_user_request'), (err, resp) ->
            expect(err).to equal null
            async.resolve()
        it 'the user', (async) ->
          @subject create('create_user_request'), (err, resp) ->
            expect(resp.user.id).to exist
            async.resolve()
        it 'the user with matchers', (async) ->
          @subject new CreateUserRequest( mail: 'server@test.com', matchers: [1,2,3]), (err, resp) ->
            expect(resp.user.id).to exist
            expect(resp.user.matchers.length).to equal 3
            async.resolve()
