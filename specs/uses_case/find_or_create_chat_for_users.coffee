require '../support/factories/repositories/chatRespositoryMongoDB'
require '../support/factories/repositories/userRespositoryMongoDB'
users = require '../support/fixtures/users'


FindOrCreateChatForUsersUseCase = require '../../src/uses_case/find_or_create_chat_for_users'
FindOrCreateChatForUsersRequest = require '../../src/uses_case/find_or_create_chat_for_users/request'
FindOrCreateChatForUsersResponse = require '../../src/uses_case/find_or_create_chat_for_users/response'

describe FindOrCreateChatForUsersUseCase, ->
  withArguments create 'chatRepositoryMongoDB'

  it -> should exist
  itsInstance -> should exist
  
  whenPass ->
    describe '::execute', ->
      usersCreated = null; req = null
      before (promise) ->
        dropDatabaseHelper ->
          saveDocumentDatabaseHelper users, 'users', (err, docs) ->
            usersCreated = docs
            req = new FindOrCreateChatForUsersRequest (doc._id.toString() for doc in docs[0..1]), create('userRepositoryMongoDB')
            req.done -> promise.resolve()

      after (promise) ->
        dropDatabaseHelper -> promise.resolve()

      context 'Passing a valid req to the UC', ->
        it 'The error must be null', (async) ->
          @subject req, (err, response) ->
            expect(err).to be null
            async.resolve()

        it 'the response must be a FindOrCreateChatForUsersResponse', (async) ->
          @subject req, (err, response) ->
            expect(response).to exist
            expect(response instanceof FindOrCreateChatForUsersResponse).to be true
            async.resolve()


