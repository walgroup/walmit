require '../support/factories/repositories/userRespositoryMongoDB'
CheckEmailPasswordUseCase = require '../../src/uses_case/check_email_password'
CheckEmailPasswordRequest = require '../../src/uses_case/check_email_password/request'

describe CheckEmailPasswordUseCase, ->
  withArguments create 'userRepositoryMongoDB'

  it -> should exist
  itsInstance -> should exist

  describe '::execute', ->
    before (async) ->
      dropDatabaseHelper ->
        saveDocumentDatabaseHelper {email: 'test@server.com', password: '1234'}, 'users', (err, doc) ->
          async.resolve()

    after (async) ->
      dropDatabaseHelper -> async.resolve()
    context 'llamandole con una instancia valida de CheckEmailPasswordRequest', ->
      it 'debe llamarse al callback con true', (async) ->
        req = new CheckEmailPasswordRequest('test@server.com', '1234')
        @subject req, (err, user) ->
          expect(err).to equal null
          expect(user.id()).to exist
          async.resolve()
    context 'llamandole con una instancia NO valida de CheckEmailPasswordRequest', ->
      it 'debe llamarse al callback con false', (async) ->
        req = new CheckEmailPasswordRequest('test@server.com', '123456')
        @subject req, (err, user) ->
          expect(err).to equal null
          expect(user).not.to exist
          async.resolve()
