require '../support/factories/uses_case/send_validation_email_user/request'

SendValidationEmailUseCase = require '../../src/uses_case/send_validation_email_user'

describe SendValidationEmailUseCase, ->

  it -> should exist
  itsInstance -> should exist

  describe '::execute', ->
    context 'llamado con un instancia de SendValidationEmailRequest', ->
      it 'should send the email', (async) ->
        @subject create('send_validation_email_user_request'), (err) ->
          expect(err).to equal null
          async.resolve()
