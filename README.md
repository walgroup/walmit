![Status Codeship](https://www.codeship.io/projects/638e4940-01be-0131-f45e-0e8b86ef01dd/status "Status Codeship")

> MongoDB como requisito. La url se puede modificar en el fichero config.coffee

## Instalacion ##

```
$ npm install grunt-cli -g
$ npm install
$ grunt
```

## Documentacion ##

```
$ grunt doc
```

Visitar http://localhost:8080

## Lanzar los tests ##

```
$ npm test
```
