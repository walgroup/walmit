/**
 *  Implementacion de los repositorios que van a ser expuestos
 * */
module.exports.repositories = {

  /**
   *  User Entity repository Implementacion
   * */
  user: {
    UserRepositoryMongoDB: require('./lib/repositories/user/userRepositoryMongoDB')
  },

  /**
   * Chat Entity repository implementacion
   * */
  chat: {
    ChatRepositoryMongoDB: require('./lib/repositories/chat/chatRepositoryMongoDB')
  }
};

/**
 *  Casos de uso que van a ser expuestos 
 * */
module.exports.uses_case = {

  /**
   * Crear un usuario
   * */
  create_user: {
    CreateUserUseCase: require('./lib/uses_case/create_user'),
    CreateUserRequest: require('./lib/uses_case/create_user/request'),
    CreateUserResponse: require('./lib/uses_case/create_user/response'),
  },

  /**
   *  Validar email de usuario
   * */
  send_validation_email_user: {
    SendValidationEmailUseCase: require('./lib/uses_case/send_validation_email_user/'),
    SendValidationEmailRequest: require('./lib/uses_case/send_validation_email_user/request'),
    SendValidationEmailResponse: require('./lib/uses_case/send_validation_email_user/response'),
  },

  /**
   * Check hash email de usuario
   * */
  check_hash_email_validation: {
    CheckHashValidationUseCase: require('./lib/uses_case/check_hash_email_validation/'),
    CheckHashValidationRequest: require('./lib/uses_case/check_hash_email_validation/request')
  },

  /**
   * Check email/password de usuario
   * */
  check_email_password: {
    CheckEmailPasswordUseCase: require('./lib/uses_case/check_email_password/'),
    CheckEmailPasswordRequest: require('./lib/uses_case/check_email_password/request')
  },

  /**
   * Devuelve los usuarios que concuerdan con el criterio de busqueda de un user
   * */
  return_valids_candidates_for_user: {
    ReturnsValidsCandidatesForUserUseCase: require('./lib/uses_case/return_valids_candidates_for_user/'),
    ReturnsValidsCandidatesForUserRequest: require('./lib/uses_case/return_valids_candidates_for_user/request'),
    ReturnsValidsCandidatesForUserResponse: require('./lib/uses_case/return_valids_candidates_for_user/response'),
  },

  /**
   * Devuelve o crea un chat dados dos users
   * */
  find_or_create_chat_for_users: {
    FindOrCreateChatForUsersUseCase: require('./lib/uses_case/find_or_create_chat_for_users/'),
    FindOrCreateChatForUsersRequest: require('./lib/uses_case/find_or_create_chat_for_users/request'),
    FindOrCreateChatForUsersResponse: require('./lib/uses_case/find_or_create_chat_for_users/response')
  }
};
