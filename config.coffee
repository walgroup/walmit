config =
  mongo: {}
  mailer:
    nodemailer:
      smtp: {}
      ses:
        user: 'hello@walmit.com'

switch process.env.NODE_ENV
  when 'test'
    config.mongo.db = 'mongo.walmit.vm/walmit_test'
    config.mailer.nodemailer.smtp.user = 'walmitcom@gmail.com'
    config.mailer.nodemailer.smtp.pass = 'w4lmit_com'
  when 'production'
    config.mongo.db = 'localhost/walmit'
  else
    config.mongo.db = 'mongo.walmit.vm/walmit_dev'
    config.mailer.nodemailer.ses.user = 'hello@walmit.com'
    config.mailer.nodemailer.smtp.user = 'walmitcom@gmail.com'
    config.mailer.nodemailer.smtp.pass = 'w4lmit_com'

module.exports = config
