secure_config =
  amazon:
    ses:
      secretAccessKey: ""
      accessKeyId: ""

module.exports = secure_config
