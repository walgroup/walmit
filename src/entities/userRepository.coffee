# @abstract
class UserRepository

  ##
  # Recibe una entidad de usuario y la salva
  # @param {User} user UserEntity
  # @param {Function} cb Callback que será ejecutado cuando se haya creado la entidad
  #
  # @example Create an user
  #   new UserRepositoryMongoDB().create userEntity, (err, user) ->
  #     //->user objeto devuelto por la base de datos
  ##
  save: (user, cb) ->
    new throw "[UserRepository#create] MustBeImplemented"

  ##
  # Obtenemos una instancia de una entidad usuario dado un id
  #
  # @param {String} id identificador de la base de datos
  # @param {Function} cb callback que tiene que devolver la instancia de usuario o el error
  # 
  # @example Obtener un usario de la BBDD
  #   
  #   new UserRepository().one "123abc", (err, user) ->
  #     if not err
  #       //-> user es una entidad usuario
  ##
  one: (id, cb) ->
    new throw "[UserRepository#one] MustBeImplemented"

  ##
  # Comprueba si el par email/password existe en la base de datos
  # @param {String} email a comprobar
  # @param {String} password a comprobar
  # @param {Funtion} cb con el documento si es encontrado o con null si no lo es. Además de un posible error
  #
  # @example Comprobar el email/password de un usuario
  #
  #   new UserRepository().check_email_password 'test@server.com', '123', (err, doc) ->
  #     throw err if err
  #     if doc then console.log('Valido!') else console.log('NO Valido!!')
  #
  ##
  check_email_password: (email, password, cb) ->
    new throw "[UserRepository#check_email_password] MustBeImplemented"

  ##
  # Dado un usuario, actualiza el listado de coincidencias y devuelve el usuario
  #
  # @param {User} user sobre el que se va a llevar la busqueda
  # @param {Function} cb con los documentos resultantes
  # @param cb {Error} err en caso de ocurrir un error
  # @param cb {Object} user objecto que representa al usuario actualizado
  # @param cb {Array} docs Array de documentos concordantes
  ##
  matchsForUser: (user, cb) ->
    new throw "[UserRepository#matchsForUser] MustBeImplemented"
