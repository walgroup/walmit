# @abstract
class ChatRepository

  
  ##
  # Devuelve una entidad Chat dando únicamente su identificador
  #
  # @param {String} id del chat
  # @param {Function} cb a se llamado cuando se haya completado la operacion de busqueda
  # @param cb {Error} err que se rellenara con cualquier posible error si fue correcto sera nulo
  # @param cb {Chat} chat la entidad {Chat} que se ha creado con los datos de la bbdd
  ##
  one: (id, cb) ->
    new throw "[ChatRepository#one] MustBeImplemented"

  ##
  # Recibe un listado de usuarios y devuelve una entidad {Chat} ya sea tras crear una nueva en bbdd o tras devolver la que ya hay
  #
  # @param {Array<User>} users listado de los usuarios participantes
  # @param {Function} cb se ejecuta con la respuesta del bbdd
  # @param cb {Error} err si existe algun error se pasa por este parametro
  # @param cb {Chat} chat es la entidad creada/devuelta segun los datos de la bbdd
  ##
  findOrCreate: (users, cb) ->
    new throw "[ChatRepository#findOrCreate] MustBeImplemented"

module.exports = ChatRepository
