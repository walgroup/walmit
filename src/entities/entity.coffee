class Entity
  ##
  # Getter y Setter generico _metodo sobrecargado_
  # @overload id(id)
  #   Setea el actual valor de id del usuario
  #   @param {String | Number | Object} valor del id
  #   @return {Boolean} indica si la actualizacion ha sido correcta
  #
  # @overload id()
  #   Devuelve al actual valor de id del User
  #   @return {String | Number | Object} valor del id
  ##
  id: (id) ->
    if id? then @content.id = id else return @content.id
    return true
module.exports = Entity
