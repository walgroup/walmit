Entity = require './entity'

class User extends Entity
  
  # @property {Number} Máximo numero de usuario coincidentes válidos
  maxMatchers: 5

  ##
  # Contructor de la entidad Usuario
  # @param {Object} content Objecto nativo con la informacion basica de un usuario
  # @option options {string} content.city ciudad de origen
  # @option options {Number} content.month mes de nacimiento
  # @option options {Number} content.year anyo de nacimiento
  # @option options {String} content.gender sexo debe ser uno de estos dos valores [M|F]
  # @option options {String} content.nick nickname
  # @option options {String} content.email email
  # @option options {Object} content.search Objecto que representa la busqueda del usuario
  # @option options {String} content.password password usado por el user
  # @option options {Boolean} content.tc Indica si el user a aceptado los terminos y condiciones
  # @option options {Array} content.matchers Lista de id de otros users que concuerdan que los criterios de busqueda
  # @option options {Array} content.forbidden Lista de id de otros users con los que nunca mas podrá contactar
  ##
  constructor: (@content = {}) ->
    @content.year = parseInt(@content.year, 10)
    @content.matchers ?= []
    @content.forbidden ?= []
    @content.id = "#{@content._id}" #FIXME: ESTO ESTA HORRIBLEMENTE MAL AQUÍ!!! la entidad no tiene por que saber nada de esto en todo caso solo mongo

  ##
  # Getter and setter genericos para los matchers del usuario
  #
  # @overload matchers(users)
  #   Setea los usuario válidos hasta un máximo de @maxMatchers y sin repetir
  #   @param {Array<User>} listado de todos los usuario coincidentes en la BBDD
  #   @return {Boolean} indica si la operación a sido satisfactoria
  #
  # @overload matchers()
  #   Devuelve el listado de matchers para ese usuario
  #   @return {Array<String>} listado de ids de los usuarios
  ##
  matchers: (users) ->
    if users?
      @content.matchers.push user.id() for user in users when @content.matchers.length < @maxMatchers and user.content.matchers.indexOf(user.id()) is -1
      return yes
    else
      return @content.matchers
    
module.exports = User
