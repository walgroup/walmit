Entity = require './entity'

class Chat extends Entity

  ##
  # Constructor de la entidad Chat
  # @param {Object} content Objeto nativo que contiene toda la información del Chat
  # @option options {Array<String>} content.chatters los participantes del chat.
  # @option options {Array<Object>} content.messages los mensajes enviados al chat en orden de llegada
  #   {body: {String}, date: {Date}, from: {String}, to: {String}, confirm: {Boolean}}
  ##
  constructor: (@content) ->
    @content.id = @content._id.toString() #FIXME: Igualmente horrible!!!!!

module.exports = Chat

