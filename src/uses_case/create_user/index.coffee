UseCase = require '../useCase'
User = require '../../entities/user'
UserRepository = require '../../repositories/user/userRepositoryMongoDB'
CreateUserResponse = require './response'
config = require '../../../config'

##
# Caso de uso para la creacion de un usuario en la base de datos
##
class CreateUserUseCase extends UseCase

  ##
  # Inyectamos el repositorio a usar como una dependencia
  # @param {UserRepository} implementacion del repositorio para usar.
  ##
  constructor: (@repository) ->
  ##
  # Ejecucion del caso de uso
  # @param {CreateUserRequest} request objeto que encapsula los datos necesarios para crear un nuevo usuario
  # @param {Function} cb funcion para ser llamada cuando se haya creado el usuario, (err, CreateUserRespose)
  #
  # @example Creacion de un usuario
  #   req = new CreateUserRequest nickname: 'nick', year: 1981
  #   CreateUserUseCase.execute req, (err, response) ->
  #     throw new Exception 'Error' unless err?
  #     console.log response.user.id //=> '123456'
  ##
  execute: (request, cb) ->
    user = new User request.user
    @repository.save user, (err, user) ->
      process.nextTick -> cb err, new CreateUserResponse(user)

module.exports = CreateUserUseCase
