Request = require '../request'

class CreateUserRequest extends Request
  ##
  # Recibe los valores necesarios para ser pasados al contructor de la entidad User
  # @see User#constructor
  ##
  constructor: (@user) ->

module.exports = CreateUserRequest
