Response = require '../response'

class CreateUserResponse extends Response

  ##
  # Recibe la nueva nueva entidad creada y devuelve los valores adecuados
  # @param {User} user entidad creada del nuevo usuario
  # @return {Object} Valores que representan al nuevo usuario creado
  ##
  constructor: (user) ->
   @user = user.content

module.exports = CreateUserResponse
