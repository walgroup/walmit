Request = require '../request'

class SendValidationEmailRequest extends Request
  ##
  # Recibe los parámetros necesarios para crear el hash y enviar el mail
  # @param {String} id identificador del user
  # @param {String} email mail del user al que se le va a enviar el hash de validacion
  ##
  constructor: (@id, @mail) ->

module.exports = SendValidationEmailRequest
