{amazon} = require '../../../secure_config'
jade = require 'jade'
HashMail = require '../../libs/hash'
Mailer = require('../../libs/mailer').MailerNodeMailerSES
UseCase = require '../useCase'

class SendValidationEmailUseCase extends UseCase

  ##
  # Constructor de la clase
  ##
  constructor: ->

  ##
  # Ejecutamos el caso de uso pasandole el mail al que enviarlo.
  # @param {SendValidationEmailRequest} request Objeto de la request del caso de uso
  # @param {Function} cb funcion que sera llamada cuando se haya producido correctamente el envio. 
  #
  # @example Como enviar un mail de vlaidacion a una cuenta de mail
  #
  #   new SendValidationEmailUseCase().execute new SendValidationEmailRequest('test@server.com'), (err) ->
  #     if err? then console.log('Algo fue mal') else console.log('Email enviado')
  ##
  execute: (request, cb) ->
    hash = new HashMail().create request.id, request.mail
    html = jade.renderFile "#{__dirname}/../../../src/templates/validate_email.jade", hash: hash
    new Mailer(amazon.ses.accessKeyId, amazon.ses.secretAccessKey).send request.mail, 'Confirma tu email para Walmit!', html, (err) ->
      process.nextTick -> cb err
module.exports = SendValidationEmailUseCase
