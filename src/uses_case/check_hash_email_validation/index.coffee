HashMail = require '../../libs/hash'
UseCase = require '../useCase'

class CheckHashValidationUseCase extends UseCase

  ##
  # Constructor de la clase
  ##
  constructor: ->
  
  ##
  # Ejecuta el caso de uso
  # @param {CheckHashValidationRequest} Request con el string que tenemos que validar
  # @param {Function} cb función que se ejecuta con la respuesta de la validación
  #
  # @example Validar un hash de email
  #
  #   new CheckHashValidationUseCase().execute new CheckHashValidationRequest('123abc'), (ok, cb) ->
  #     if ok
  #       el hash es valido y el mail ha si validado y actualizado el user en BBDD
  #     else
  #       Algo fue mal.
  ##
  execute: (request, cb) ->
    new HashMail().check request.hash, cb

  
  
module.exports = CheckHashValidationUseCase
