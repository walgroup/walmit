Request = require '../request'

class CheckHashValidationRequest extends Request
  ##
  # Recibe el hash para ser validado
  # @param {String} hash cadena que compone el hash para ser validado
  ##
  constructor: (@hash) ->

module.exports = CheckHashValidationRequest

