UseCase = require '../useCase'
FindOrCreateChatForUsersResponse = require './response'
config = require '../../../config'

##
# Caso de uso para la creacion/devolucion de un chat en la base de datos
##
class FindOrCreateChatForUsersUseCase extends UseCase

  ##
  # Inyectamos el repositorio a usar como una dependencia
  # @param {ChatRepository} repository implementacion del repositorio para usar.
  ##
  constructor: (@repository) ->

  ##
  # Ejecutamos el caso de uso para obtener un chat que ya exista y que hayamos creado
  #
  # @param {FindOrCreateChatForUsersRequest} request objeto con los {User} que participan en el chat
  # @param {Function} cb callback que será ejecutado con la respuesta de la ejecucion del UC
  # @param cb {Error} err en caso de haber un error aquí se presentaría
  # @param cb {FindOrCreateChatForUsersResponse} response objecto que debe contener la entidad del chat
  #
  # @example Uso del caso de uso
  #   req = new FindOrCreateChatForUsersRequest('123abc', '456def')
  #   repo = new ChatRepository()
  #   new FindOrCreateChatForUsersUseCase(repo).execute req, (err, response) ->
  #     console.log response.chat
  ##
  execute: (request, cb) ->
    request.done (first, second) =>
      @repository.findOrCreateByUsers [first, second], (err, chat) ->
        cb err, new FindOrCreateChatForUsersResponse(chat)

module.exports = FindOrCreateChatForUsersUseCase
