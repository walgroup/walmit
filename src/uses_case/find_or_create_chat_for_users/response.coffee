Response = require '../response'

class FindOrCreateChatForUsersResponse extends Response

  ##
  # Constructor de la respuesta para el caso de uso de crear un chat
  ##
  constructor: (@chat) ->

module.exports = FindOrCreateChatForUsersResponse

