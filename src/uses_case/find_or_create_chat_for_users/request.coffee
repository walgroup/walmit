$ = require 'jquery-deferred'
User = require '../../entities/user'
Request = require '../request'

class FindOrCreateChatForUsersRequest extends Request
  ##
  # Recibe los ids de los usuarios que forman parte del chat
  #
  # @property {Array<String>} users listado de los usuarios que participan en el chat
  # @property {UserRepository} repository que vamos a usar para crear las entidades User
  #
  # @example Uso de una request de este tipo
  #
  #   new FindOrCreateChatForUsersRequest('123abc', 456def).done (firstUser, secondUser) ->
  #     console.log firstUser, secondUser
  ##
  constructor: (@users, repository) ->
    deferred = $.Deferred()
    $.when.apply(this, @users.map (user) -> repository.one(user)).done deferred.resolve
    return deferred.promise()

module.exports = FindOrCreateChatForUsersRequest
