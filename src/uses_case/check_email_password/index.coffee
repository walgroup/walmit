UseCase = require '../useCase'

##
# Caso de uso que dice si el par email y password es válido
##
class CheckEmailPasswordUseCase extends UseCase

  ##
  # Inyectamos la dependencia del repositorio a usar
  # @param {UserRepository} implementacion del repositorio para usar
  ##
  constructor: (@repository) ->

  ##
  # Ejecución del caso de uso
  # @param {CheckEmailPasswordRequest} request object con el par a comprobar
  #
  # @example Comprobar un email y password
  #
  #   req = new CheckEmailPasswordRequest 'user@server.com', '123'
  #   new CheckEmailPasswordUseCase.execute req, (err, valid) ->
  #     throw err if err
  #     if valid
  #       //=> Todo guay
  ##
  execute: (req, cb) ->
    @repository.check_email_password req.email, req.password, (err, user) ->
      cb err, user

module.exports = CheckEmailPasswordUseCase
