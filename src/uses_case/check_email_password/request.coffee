Request = require '../request'

class CheckEmailPasswordRequest extends Request
  ##
  # constructor para una peticion de comprobacion del email/password
  #
  # @param {String} email para comprobar
  # @param {String} password a comprobar
  ##
  constructor: (@email, @password) ->

module.exports = CheckEmailPasswordRequest
