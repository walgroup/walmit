UseCase = require '../useCase'
ReturnsValidsCandidatesForUserResponse = require './response'

##
# Caso de uso para obtener los candidatos adecuados para chatear. Dependiendo del criterio de busqueda del usuario
##
class ReturnsValidsCandidatesForUserUseCase extends UseCase

  ##
  # Inyectamos el repositorio sobre el que se hará la busqueda de candidatos.
  # @param {UserRepository} repository sobre el que debemos hacer la búsqueda 
  ##
  constructor: (@repository) ->

  ##
  # Ejecutamos el caso de uso con la request adecuada
  # @param {ReturnsValidsCandidatesForUserRequest} req sobre el que se hará la busqueda
  # @param {Function} cb Para ejecutar una vez que se obtengan los resultados. Debe devolver aun array de {User}
  # @param cb {Error} err any error when try to do the operation
  # @param cb {Array<User>} users conjunto de usuario que pasan los filtros de busqueda.
  #
  # @example Ejecuacion del caso de uso
  #
  #   req = new ReturnsValidsCandidatesForUserRequest {email:'user@server.com', _id:'123abc'} // Y todos los demas parámetros para construir un User
  #   req = new ReturnsValidsCandidatesForUserRequest '123abc', repository // Solo un string que representa el id del user
  #   new ReturnsValidsCandidatesForUserCaseUse req, (err, users) ->
  #     throw err if err
  #     users.forEach (user) ->
  #       console.log user.id()
  ##
  execute: (req, cb) ->
    req.done (user) =>
      @repository.matchsForUser user, (err,  users, user) ->
        cb err, new ReturnsValidsCandidatesForUserResponse users, user
    
module.exports = ReturnsValidsCandidatesForUserUseCase
