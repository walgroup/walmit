Response = require '../response'
User = require '../../entities/user'

class ReturnsValidsCandidatesForUserResponse extends Response

  # @property {Array<User>} Usuarios que conforman la respuesta del caso de uso
  @users: []
  ##
  # Recibe la nueva nueva entidad creada y devuelve los valores adecuados
  # @param {Array<User>} users lista de usuarios que coinciden con la busqueda. Todos los de la BBDD
  # @param {User} user que contiene un listado con los unicos users validos en este momento
  ##
  constructor: (@users, @user) ->

module.exports = ReturnsValidsCandidatesForUserResponse
