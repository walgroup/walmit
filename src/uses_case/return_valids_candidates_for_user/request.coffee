Q = require 'q'
User = require '../../entities/user'
Request = require '../request'

class ReturnsValidsCandidatesForUserRequest extends Request

  ##
  # constructor
  #
  # Recibe los valores necesarios para ser pasados al contructor de la entidad User o un id de un User
  # Esta clase se comporta como una promesa, para asegurar que siempre devuelve un user válido
  #
  # @param {Object|String} user pude ser un objeto con todos los datos necesarios para construir una instancia {User} o únicamente el id de una user 
  # @param {UserRepository} repository (OPTCIONAL). Solo es necesario enviarlo cuando _user_ es un String representando un id
  # @example Uso de una request mediante promesas
  #
  #   new ReturnsValidsCandidatesForUserRequest('1234', repository).done (user) -> console.log user
  #   new ReturnsValidsCandidatesForUserRequest({mail: 'user@server.com', nick: 'pepe', ...}).done (user) -> console.log user
  ##
  constructor: (user, repository) ->
    deferred = Q.defer()
    if typeof user is 'string'
      repository.one user, (err, doc) =>
        unless err
          deferred.resolve doc
    else
      deferred.resolve(new User(user))
    return deferred.promise


module.exports = ReturnsValidsCandidatesForUserRequest
