$ = require 'jquery-deferred'
ChatRepository = require '../../entities/chatRepository'
Chat = require '../../entities/chat'
mongojs = require 'mongojs'
config = require '../../../config'

class ChatRepositoryMongoDB extends ChatRepository

  # Constructor del repositorio usando mongoDB
  # @param {String} db cadena de conexion de mongodb. Por defecto sera la configurada en el config
  constructor: (db = config.mongo.db) ->
    @db = mongojs db, ['chats']
    @db.chats.ensureIndex {chatters: 1}, {background: true, name: 'chatters'}

  # @see ChatRepository#one
  one: (id, cb = ->) ->
    deferred = $.Deferred()
    @db.chats.findOne _id: mongojs.ObjectId(id), (err, doc) ->
      unless err then deferred.resolve(new Chat(doc)) else deferred.reject()
      cb err, new Chat(doc)
    return deferred.promise()

  # @see ChatRepository#findOrCreate 
  findOrCreateByUsers: (users, cb) ->
    chatters = users.map (user) -> user.id()
    query =
      chatters:
        $all: chatters
    @db.chats.findOne query, (err, chat) =>
      return cb(err) if err
      return cb(null, new Chat(chat)) if chat
      @db.chats.save chatters: chatters, messages: [], (err, doc) ->
        return cb(err) if err
        cb null, new Chat(doc)

module.exports = ChatRepositoryMongoDB

