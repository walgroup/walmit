$ = require 'jquery-deferred'
UserRepository = require '../../entities/userRepository'
User = require '../../entities/user'
mongojs = require 'mongojs'
config = require '../../../config'

class UserRepositoryMongoDB extends UserRepository

  # Constructor del repositorio usando mongoDB
  # @param {String} db cadena de conexion de mongodb. Por defecto sera la configurada en el config
  constructor: (db = config.mongo.db) ->
    @db = mongojs db, ['users']
    @db.users.ensureIndex {year: -1, city: 1, gender: 1}, {background: true, name: 'matcher'}
  
  # @see UserRepository#save
  save: (user, cb) ->
    @db.users.save user.content, (err, doc) ->
      cb(err, user) if user.id doc._id

  # @see UserRepository#one
  one: (id, cb = ->) ->
    deferred = $.Deferred()
    @db.users.findOne _id: mongojs.ObjectId(id), (err, doc) ->
      unless err then deferred.resolve(new User(doc)) else deferred.reject()
      cb err, new User(doc)
    return deferred.promise()
  
  # @see UserRepository#check_email_password
  check_email_password: (email, password, cb) ->
    @db.users.findOne email: email, password: password, (err, doc) ->
      user = if doc then new User(doc) else null
      user?.id doc._id
      cb err, user
      
  # @see UserRepository#matchsForUser
  matchsForUser: (user, cb) ->
    if user?.content?.search
      year = new Date().getFullYear()
      [min_age, max_age] =[year - user.content.search.year_from, year - user.content.search.year_to]
      query =
        city: user.content.search.city
        year:
          $gte: max_age
          $lte: min_age
        gender: user.content.search.gender
      @db.users.find query, (err, docs) => # Todos los users concoordantes
        users = (new User(doc) for doc in docs)
        user.matchers users
        @save user, (err, user) ->
          cb err, users, user
    else
      cb null, []

module.exports = UserRepositoryMongoDB
