{mailer} = require '../../../config'
MailerNodeMailer = require './mailer_nodemailer'
nodemailer = require 'nodemailer'

##
# Clase para enviar mails a los usuarios.
##

class MailerNodeMailerSES extends MailerNodeMailer

  ##
  # Contructor de la Clase
  # @param {String} id AWSAccessKeyID
  # @param {String} key AWSSecretKey
  ##
  constructor: (@id, @key) ->
    @from = "Walmit.com <#{mailer.nodemailer.ses.user}>"
    @transport = nodemailer.createTransport "SES", AWSAccessKeyID: @id, AWSSecretKey: @key

module.exports = MailerNodeMailerSES



