{mailer} = require '../../../config'
Mailer = require './mailer'
nodemailer = require 'nodemailer'

##
# Clase para enviar mails a los usuarios.
##

class MailerNodeMailer extends Mailer

  ##
  # @see Mailer#send
  ##
  send: (email, subject, content, cb, html = true) ->

    options = from: @from, to: email, subject: subject
    if html then  options.html = content else options.text = content

    @transport.sendMail options, (err, response) ->
      process.nextTick -> cb(err)

module.exports = MailerNodeMailer

