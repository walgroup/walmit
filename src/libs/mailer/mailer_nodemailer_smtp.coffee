{mailer} = require '../../../config'
MailerNodeMailer = require './mailer_nodemailer'
nodemailer = require 'nodemailer'

##
# Clase para enviar mails a los usuarios.
##

class MailerNodeMailerSMTP extends MailerNodeMailer

  ##
  # Contructor de la Clase
  # @param {String} service Indica el servicio que ha de usar nodemailer. Por defecto es _GMAIL_
  # @param {Object} auth objecto de autentificacion de servicio. Por defecto toma los parámetros del fichero de configuracion
  # @option options {string} auth.user El usuario del servicio
  # @option options {string} auth.pass password del servicio
  ##
  constructor: (@service = "Gmail", @auth = {user: mailer.nodemailer.smtp.user, pass: mailer.nodemailer.smtp.pass}) ->
    @from = "Walmit.com <#{mailer.nodemailer.smtp.user}>"
    @transport = nodemailer.createTransport "SMTP", {service: @service, auth: @auth}

module.exports = MailerNodeMailerSMTP


