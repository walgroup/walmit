##
# Clase abstracta que define el interfaz de un mailer
# @abstract
##
class Mailer
  
  ##
  # Metodo encargado de enviar el mail
  # @param {String} email dirrecion de envio
  # @param {String} subject Asunto del mail
  # @param {String} content contenido a enviar
  # @param {Function} cb callback que sera ejecutado cuando el mail haya sido enviado
  # @param {Boolean} html indica si el contenido es html o texto plano
  ##
  send: (email, subject, content, cb, html = true) ->
    throw new Error '[Mailer::send] este metodo abstracto'


module.exports = Mailer
