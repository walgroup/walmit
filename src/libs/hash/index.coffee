crypto = require 'crypto'
Repository = require '../../repositories/user/userRepositoryMongoDB'

##
# clase encargada de encapsular la creación y comprobación del HASH de validación del usuario.
# inspirado en: http://stackoverflow.com/questions/6953286/node-js-encrypting-data-that-needs-to-be-decrypted
##
class HashMail

  # @property {String} caracter usado para devidir la cadena
  split: '#'

  ##
  # Contructor donde puedes cambiar tanto el algoritmo como la key
  # @param {String} algorithm algoritmo usado para la encriptacion, cualquiera soportado por ssl. Por defecto _aes256_
  # @param {String} key base de la encriptacion. Por defecto *Un mundo feliz*
  ##
  constructor: (@algorithm = "aes256", @key = "Un mundo feliz") ->
     @cipher = crypto.createCipher @algorithm, @key
     @decipher = crypto.createDecipher @algorithm, @key

  ##
  # Método de clase que toma una instancia de una entidad de usuario y devuelve un hash válido
  # @param {User} user entidad usuario sobre la que crear el hash
  # @return {String} Hash generado para ese usuario
  ##
  create: (id, email) ->
    @cipher.update("#{id}#{@split}#{email}", 'utf8', 'hex') + @cipher.final('hex')


  ##
  # Método para comprobar que el hash es un hash válido
  # @param {String} hash cadena para ser validada
  # @param {Function} cb callback con la respuesta de la validación
  # @return {Boolean} True si es válido y false en caso contrario
  # @example Validar un hash
  #   
  #   new HashMail().check "123abc", (ok) ->
  #     if ok
  #       // Validacion correcta
  #     else
  #       // validacion fallida
  ##
  check: (hash, cb) ->
    try
      root = @decipher.update(hash, 'hex', 'utf8') + @decipher.final('utf8')
    catch err
      root = false
    if root
      [id, email] = root.split @split
      new Repository().one id, (err, user) ->
        process.nextTick -> cb( user.content.email is email )
    else
      process.nextTick -> cb(no)


module.exports = HashMail
