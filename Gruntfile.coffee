module.exports = (grunt) ->
  grunt.initConfig
    bgShell:
      doc:
        cmd: "node_modules/.bin/codo src/entities/ src/repositories/ src/uses_case/ src/libs/"
      doc_server:
        cmd: "node_modules/.bin/codo --server"
      test:
        cmd: "NODE_ENV=test node_modules/.bin/spectacular test -c -d specs/**/*.coffee specs/**/**/*.coffee"
    clean:
      doc: ['doc']
      compile: ['lib']
    coffee:
      config:
        files: 'config.js': 'config.coffee'
      secure_config:
        files: 'secure_config.js': 'secure_config.coffee'
      compile:
        expand: true,
        cwd: 'src/',
        src: ['**/*.coffee', '**/**/*.coffee', '**/**/*.*.coffee'],
        dest: 'lib/',
        ext: '.js'

  grunt.loadNpmTasks "grunt-bg-shell"
  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-coffee"

  grunt.registerTask "doc", ["clean:doc", "bgShell:doc", "bgShell:doc_server"]
  grunt.registerTask "test", ["bgShell:test"]
  grunt.registerTask "default", ["clean:compile", "coffee"]
